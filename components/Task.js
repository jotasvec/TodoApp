import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from "react-native-elements";
class Task extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.task_text}> {this.props.item.text} </Text>
        
        <TouchableOpacity onPress={() => {
                    this.props.deleteItem(this.props.item.key);
                    }} >
            <Icon
                name='check-circle'
                type='font-awesome'
                size={20}
                color='#FFF'
                
            />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 20,  

    },
    task_text: {
        fontSize:20,
        color: 'blue',
    }
});

export default Task;
