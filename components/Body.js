import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import Task from './Task';

export default class Body extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }

  render() {
    return (
      <View style={styles.container}>
        <Text> {this.props.showText} </Text>
        <FlatList
            data={this.props.showTaskList}
            renderItem={ ({item}) => <Task item={item} deleteItem={this.props.deleteItem}/> }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        flex: 8,
        backgroundColor: '#FFF',
    }
});
