import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

export default class Header extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.h_title} > ToDo Tasks </Text>
        <TextInput
            value={this.props.initText}
            placeholder='Add Task'
            onChangeText={ this.props.addText }
            onSubmitEditing={this.props.addTask}
            
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#BBB',
        justifyContent: 'center',
        // alignContent: 'center',
        alignItems: 'center',
    },
    h_title:{
        color: '#FFF',
        paddingVertical: 10,
        
    }
});
