/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, AsyncStorage, ActivityIndicator} from 'react-native';
import Header from './components/Header';
import Body from './components/Body';


export default class App extends Component {
  constructor(){
    super();
    this.state = {
      task_list: [],
      text_task: '',
      cargando: true
    };
  }

  
  //despues de montar el render se ejecuta automaticamente esta funcion, en la func se usa 
  // getTaskFroPhone para agregar la lista de tareas en el body de la APP 
  componentDidMount(){
    this.getTasksFromPhone();
  }

  // obtiene la tarea desde el header y lo agrega a la lista de tareas
  getTextFromHeader = (value) =>{
    console.log('->'+ value);
    this.setState({text_task: value});
  }

  // agrega la tarea a la lista de tareas
  addTaskFromHeader = () =>{
    const new_tasks_list = [...this.state.task_list, {text: this.state.text_task, key: Date.now()+'' }];
    this.saveTasksOnPhone(new_tasks_list);
    this.setState({
      task_list: new_tasks_list,
      text_task: ''
    });
    console.log('-->'+this.state.task_list);
  }

  // elimina la tarea seleccionada de la lista de tareas
  deleteFromTasksList = (id_task) =>{
    const new_tasks_list = this.state.task_list.filter( (task) => {return task.key !== id_task} );
    console.log('se elimina =>'+id_task);
    this.saveTasksOnPhone(new_tasks_list);
    this.setState({ task_list: new_tasks_list});
  }

  // guarda la lista de Tareas en la memoria del telefono
  saveTasksOnPhone = (tasks_array) =>{
    AsyncStorage.setItem('@ToDoAppKey:task_list', JSON.stringify(tasks_array))
    .then((value) => {
      console.log(value)
    })
    .catch((Err) => {
      console.log(Err);
    });
  }

  // se obtiene la lista de tareas guardadas en el telefono
  getTasksFromPhone = () => {
    AsyncStorage.getItem('@ToDoAppKey:task_list')
    .then((value) => {
      console.log(value);
      if(value != null){
        const tasks = JSON.parse(value);
        this.setState({
          task_list: tasks
        });
      }
    })
    .catch((Err) => {
      console.log(Err);
    });
  }

  render() {
    return (
      // usar ActivityIndicator
      // { this.state.cargando && <ActivityIndicator
      //     size="large"
      //     color="#640064"
      //   >

        <View style={styles.container}>
          <Header
            initText={this.state.text_task}
            addText={this.getTextFromHeader}
            addTask={this.addTaskFromHeader}
          />
          <Body
            // showText={this.state.text_task}
            showTaskList={this.state.task_list}
            deleteItem={this.deleteFromTasksList}
          />
        </View>

      // </ActivityIndicator>}
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  }
});

